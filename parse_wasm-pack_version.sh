#!/bin/sh

sed '/^FROM/!d;/as cargo-wasm-pack/!d' Dockerfile | tail -n 1 | cut -d':' -f2 | cut -d' ' -f1
